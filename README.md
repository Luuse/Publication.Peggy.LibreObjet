# PEGGY
## EEN OBJECT ALS GEMEENGOED
## UN OBJET EN COMMUN
## AN OBJECT IN COMMON

‘Peggy, an Object in Common’ is the result of ‘Objects in Common’, a project by Constant in Brussels and Medialab-Prado in Madrid.

**Coordination project:**
Susana Moliner, Medialab-Prado and Wendy Van Wynsberghe, Constant.

**Coordination publication:**
An Mertens, Constant

**Translation ‘Grigri Pixel, an attempt at a world in common’:**
Patrick Lennon

**Proofreading:**
Patrick Lennon, Stéphanie Vailayphiou, Wendy Van Wynsberghe (notes participants)

**Graphic Design:**
Antoine Gelgon (Atelier Bek), method: Html2Print

**Printing:**
Ronan Deriez

**Responsible Publisher:**
Constant, rue du Fortstraat 5, 1060 Brussels

-----------------------

**PARTICIPANTS / DEELNEMERS:**
Alberto Flores, An Mertens, Blanca Callén, Cesar García Saez, Danny Leen, Francisco Díaz, Francois Chasseur, Jil Theunissen, Julien Deswaef, Julien Leresteux, Kokou Elolo Amegayibo, Luc Hanneuse, Malcolm Bain, Mamadou Coulibaly, Marc Lambaerts, marthe van dessel, Mireia Juan, Modou Ngom, Paul Appert, Philippe Jadin, Sara San Gregorio, Veerle De Ridder, Wendy Van Wynsberghe, Wim Van Gool, Zineb El Fasik

**Typografie / Typographies**
Ocr-Pbi (Lise Brosseau - Antoine Gelgon) - SIL Open Font License
Crimson Text (Sebastian Kosch) - SIL Open Font License



**Licentie:**
Copyright, Constant, Peggy, Brussel, 2016. Copyleft: Dit is een vrij werk, je kan het materiaal kopiëren, veranderen en opnieuw verspreiden volgens de voorwaarden van de Free Art License: http://artlibre.org.

**Licence:**
Copyright, Constant, Peggy, Bruxelles, 2016. Copyleft : cette oeuvre est libre, vous pouvez la redistribuer et/ou la modifier selon les termes de la Licence Art Libre: http://artlibre.org.
License:
Copyright, Constant, Peggy, Brussels, 2016. Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License: http://artlibre.org.

**Make More Peggy’s**
You can still make your own version of Peggy. Download the file and instructions here: http://www.libreobjet.org> (CERN Open Hardware License). Upload your images, changed design files and documentation to your favourite on-line platform and send a link to wendy@constantvzw.org to have it published on www.capacitor.constantvzw.org.
